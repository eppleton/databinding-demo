package com.simpligility.android.helloflashlight;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import com.simpligility.android.helloflashlight.databinding.MainBinding;
/**
 * HelloFlashlight is a sample application for the usage of the Maven Android
 * Plugin. The code is trivial and not the focus of this example and therefore
 * not really documented.
 *
 * @author Manfred Moser <manfred@simpligility.com>
 */
public class HelloFlashlight extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainBinding binding = DataBindingUtil.setContentView(this, R.layout.main);
        User user = new User("Test", "User");
        binding.setUser(user);
    }
  
}
